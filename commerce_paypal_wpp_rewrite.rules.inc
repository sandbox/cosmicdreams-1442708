<?php

/**
 * Implements hook_rules_condition_info().
 */
function commerce_paypal_wpp_rewrite_rules_condition_info() {
  return array(
    'commerce_paypal_wpp_rewrite_rules_condition_order_contains_product_type' => array(
       'label' => t('Order contains a product type'),
       'group' => t('Commerce Cart'),
       'arguments' => array(
          'commerce_order' => array(
            'type' => 'commerce_order',
            'label' => t('Commerce Order'),
          ),
          'product_type' => array(
            'type' => 'text',
            'label' => t('Product Type'),
            'options list' => 'commerce_product_type_options_list',
          ),
       ),
    ),
  );
}

// Checks the order for a specific product type.
function commerce_paypal_wpp_rewrite_rules_condition_order_contains_product_type($order, $type) {
  // Look for the line items in the order
  $line_items = field_get_items('commerce_order', $order, 'commerce_line_items');

  // If no line items were found (this is highly unlikely, but possible).
  if (empty($line_items)) {
    return FALSE;
  }

  $line_items = commerce_line_item_load_multiple($line_items);

  // Search each line item for the type we want.
  foreach ($line_items as $li) {
    // Check the type
    if (!empty($li->commerce_product)) {
      // Logically, most line items will only contain ONE product reference, but its important to understand that there could be multiple.
      $products = field_get_items('commerce_line_item', $li, 'commerce_product');
      // Check that product refs were actually found.
      if (empty($products)) {
        // Move ahead to the next line item.
        continue;
      }
      // Load all products found on the line item.
      $products = commerce_product_load_multiple($products);
      // Scan over each one
      foreach ($products as $product) {
        // Check their type. If ANY match, this condition is met.
        if ($product->type == $type) {
          return TRUE;
        }
      }
    }
  }

  // We didn't find the product of that type in our cart
  return FALSE;
}
