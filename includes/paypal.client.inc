<?php

/**
 * @file
 * Implements PayPal Website Payments Pro API calls.
 */

function commerce_paypal_wpp_rewrite_address_verify_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: any business logic that needed to collect the values of EMAIL, STREET, and ZIP

  /**
   * (Required) Must be AddressVerify.
   */
  $nvp['METHOD'] = 'AddressVerify';

  /**
   * (Required) Email address of a PayPal member to verify.
   * Character length and limitations: 255 single-byte characters maximum with the input mask: ?@?.??
   */
  $nvp['EMAIL'] = $data['EMAIL'];

  /**
   * (Required) First line of the billing or shipping postal address to verify. To pass verification, the value of Street  must match the first 3 single-byte characters of a postal address on file for the PayPal member.
   * Character length and limitations: 35 single-byte characters maximum, including alphanumeric plus - , . ‘ # \. Whitespace and case of input value are ignored.
   */
  $nvp['STREET'] = $data['STREET'];

  /**
   * (Required) Postal code to verify. To pass verification, the value of Zip must match the first 5 single-byte characters of the postal code of the verified postal address for the verified PayPal member.
   * Character length and limitations: 16 single-byte characters maximum. Whitespace and case of input value are ignored.  
   */
  $nvp['ZIP'] = $data['ZIP'];

  return $nvp;
}

/**
 * Implements: Implements https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_BillOutstandingAmount
 */
function commerce_paypal_wpp_rewrite_bill_outstanding_amount_request($data) {  
  //TODO: implement Business logic
  //
  $nvp = array();

  $nvp['METHOD'] = 'BillOutstandingAmount';

  /**
   * (Required) Recurring payments profile ID returned in the CreateRecurringPaymentsProfile response.
   * NOTE:The profile must have a status of either Active or Suspended.
   * Character length and limitations: 14 single-byte alphanumeric characters. 19 character profile 
   * IDs are supported for compatibility with previous versions of the PayPal API.
   */
  $nvp['PROFILEID'] = $data['PROFILEID'];

  /*
   * (Optional) The amount to bill. The amount must be less than or equal to the current outstanding balance of the profile. 
   * If no value is specified, PayPal attempts to bill the entire outstanding balance amount.
   * Character length and limitations: Value is a positive number which cannot exceed $10,000 USD in any currency. It
   * includes no currency symbol. It must have 2 decimal places, the decimal separator must be a period (.), and the optional
   * thousands separator must be a comma (,).
   */
  $nvp['AMT'] = $data['AMT'];

  /**
   * (Optional) The reason for the non-scheduled payment. For profiles created using Express Checkout, this message is 
   * included in the email notification to the buyer for the non-scheduled payment transaction, and can also be seen by both 
   * you and the buyer on the Status History page of the PayPal account.
   */
  $nvp['NOTE'] = $data['NOTE'];

  return $nvp;
}

function commerce_paypal_wpp_rewrite_callback_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_CallbackAPI

  return $nvp;
}

function commerce_paypal_wpp_rewrite_create_recurring_payments_profile_request($data, $payment_method, $pane_form, $pane_values, $order, $charge) {
  // implement business logic
  $description = array();
  $nvp = array();
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Prepare the billing address for use in the request.
  $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();

  if (empty($billing_address['first_name'])) {
    $name_parts = explode(' ', $billing_address['name_line']);
    $billing_address['first_name'] = array_shift($name_parts);
    $billing_address['last_name'] = implode(' ', $name_parts);
  }

  // Build a description for the order.
  foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {
    if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())) {
      $description[] = round($line_item_wrapper->quantity->value(), 2) . 'x ' . $line_item_wrapper->line_item_label->value();
    }
  }

  $nvp['METHOD'] = "CreateRecurringPaymentsProfile";

  /**   
   * A timestamped token, the value of which was returned in the response to the first call to SetExpressCheckout. You can
   * also use the token returned in the SetCustomerBillingAgreement response. Either this token or a credit card number is 
   * required. If you include both token and credit card number, the token is used and credit card number is ignored Call 
   * CreateRecurringPaymentsProfile once for each billing agreement included in SetExpressCheckout request and use the same 
   * token for each call. Each CreateRecurringPaymentsProfile request creates a single recurring payments profile.
   * NOTE:Tokens expire after approximately 3 hours.
   * We don't use token we use credit card number instead   
   */
  //$nvp['TOKEN'] = isset($data['TOKEN']) ? $data['TOKEN'] : '';
  

  /**
   * Recurring Payments Profile Details Fields
   */

  /**
   * (Optional) Full name of the person receiving the product or service paid for by the recurring payment. If not present, the name in the buyer.s PayPal account is used.
   * Character length and limitations: 32 single-byte characters
   */
  // $nvp['SUBSCRIBERNAME'] = isset($data['SUBSCRIBERNAME']) ? $data['SUBSCRIBERNAME'] : '';

  /**
   * (Required) The date when billing for this profile begins.
   * NOTE:The profile may take up to 24 hours for activation.
   * Character length and limitations: Must be a valid date, in UTC/GMT format
   */
  $nvp['PROFILESTARTDATE'] = isset($data['PROFILESTARTDATE']) ? $data['PROFILESTARTDATE'] : gmdate("c");

  /**
   * (Optional) The merchant.s own unique reference or invoice number.
   * Character length and limitations: 127 single-byte alphanumeric characters
   */
  //$nvp['PROFILEREFERENCE'] = isset($data['PROFILEREFERENCE']) ? $data['PROFILEREFERENCE'] : '';

  /**
   * Schedule Details Fields
   */

  /**
   * (Required) Description of the recurring payment.
   * NOTE:You must ensure that this field matches the corresponding billing agreement description included in the SetExpressCheckout request.
   * Character length and limitations: 127 single-byte alphanumeric characters
   */
  $nvp['DESC'] = isset($data['DESC']) ? $data['DESC'] : substr(implode(', ', $description), 0, 127);

  /**
   * (Optional) Number of scheduled payments that can fail before the profile is automatically suspended. An IPN message is sent to the merchant when the specified number of failed payments is reached.
   * Character length and limitations: Number string representing an integer
   */
  $nvp['MAXFAILEDPAYMENTS'] = isset($data['MAXFAILEDPAYMENTS']) ? $data['MAXFAILEDPAYMENTS'] : 0;

  /**
   * (Optional) Indicates whether you would like PayPal to automatically bill the outstanding balance amount in the next billing cycle. The outstanding balance is the total amount of any previously failed scheduled payments that have yet to be successfully paid. It is one of the following values:
   * - NoAutoBill . PayPal does not automatically bill the outstanding balance.
   * - AddToNextBilling . PayPal automatically bills the outstanding balance.
   */
  $nvp['AUTOBILLOUTAMT'] = isset($data['AUTOBILLOUTAMT']) ? $data['AUTOBILLOUTAMT'] : 'AddToNextBilling';

  /**
   * Billing Period Details Fields
   */

  /**
   * (Required) Unit for billing during this subscription period. It is one of the following values:
   * - Day, Week, SemiMonth, Month, Year
   * For SemiMonth, billing is done on the 1st and 15th of each month.
   * NOTE: The combination of BillingPeriod and BillingFrequency cannot exceed one year.
   */
  $nvp['BILLINGPERIOD'] = isset($data['BILLINGPERIOD']) ? $data['BILLINGPERIOD'] : 'Year';

  /**
   * (Required) Number of billing periods that make up one billing cycle.
   * The combination of billing frequency and billing period must be less than or equal to one year. For example, if the billing cycle is Month, the maximum value for billing frequency is 12. Similarly, if the billing cycle is Week, the maximum value for billing frequency is 52.
   * NOTE:If the billing period is SemiMonth., the billing frequency must be 1.
   */
  $nvp['BILLINGFREQUENCY'] = isset($data['BILLINGFREQUENCY']) ? $data['BILLINGFREQUENCY'] : 1;

  /**
   * (Optional) Number of billing cycles for payment period.
   * For the regular payment period, if no value is specified or the value is 0, the regular payment period continues until the profile is canceled or deactivated.
   * For the regular payment period, if the value is greater than 0, the regular payment period will expire after the trial period is finished and continue at the billing frequency for TotalBillingCycles cycles.
   */
  //$nvp['TOTALBILLINGCYCLES'] = isset($data['TOTALBILLINGCYCLES']) ? $data['TOTALBILLINGCYCLES'] : '';

  /**
   * (Required) Billing amount for each billing cycle during this payment period. This amount does not include shipping and tax amounts.
   * NOTE:All amounts in the CreateRecurringPaymentsProfile request must have the same currency.
   * Character length and limitations: Value is a positive number which cannot exceed $10,000 USD in any currency. It includes no currency symbol. It must have 2 decimal places, the decimal separator must be a period (.), and the optional thousands separator must be a comma (,).
   */
  $nvp['AMT'] = isset($data['AMT']) ? $data['AMT'] : commerce_currency_amount_to_decimal(commerce_currency_convert($charge['amount'], $charge['currency_code'], $payment_method['settings']['currency_code']), $charge['currency_code']);

  /**
   * Unit for billing during this subscription period; required if you specify an optional trial period. It is one of the following values:
   *  - Day, Week, SemiMonth, Month, Year
   * For SemiMonth, billing is done on the 1st and 15th of each month.
   * NOTE: The combination of BillingPeriod and BillingFrequency cannot exceed one year.
   */
  //$nvp['TRIALBILLINGPERIOD'] = isset($data['TRIALBILLINGPERIOD']) ? $data['TRIALBILLINGPERIOD'] : '';

  /**
   * Number of billing periods that make up one billing cycle; required if you specify an optional trial period.
   * The combination of billing frequency and billing period must be less than or equal to one year. For example, if the billing cycle is Month, the maximum value for billing frequency is 12. Similarly, if the billing cycle is Week, the maximum value for billing frequency is 52.
   * NOTE: If the billing period is SemiMonth., the billing frequency must be 1.
   */
  //$nvp['TRIALBILLINGFREQUENCY'] = isset($data['TRIALBILLINGFREQUENCY']) ? $data['TRIALBILLINGFREQUENCY'] : '';

  /**
   * (Optional) Number of billing cycles for trial payment period.
   */
  //$nvp['TRIALTOTALBILLINGCYCLES'] = isset($data['TRIALTOTALBILLINGCYCLES']) ? $data['TRIALTOTALBILLINGCYCLES'] : '';

  /**
   * Billing amount for each billing cycle during this payment period; required if you specify an optional trial period. This amount does not include shipping and tax amounts.
   * NOTE: All amounts in the CreateRecurringPaymentsProfile request must have the same currency.
   * Character length and limitations: Value is a positive number which cannot exceed $10,000 USD in any currency. It includes no currency symbol. It must have 2 decimal places, the decimal separator must be a period (.), and the optional thousands separator must be a comma (,).
   */
  //$nvp['TRIALAMT'] = isset($data['TRIALAMT']) ? $data['TRIALAMT'] : '';

  /**
   * (Required) Currency code (default is USD).
   * Character length and limitations: 3 single-byte characters
   */
  $nvp['CURRENCYCODE'] = isset($data['CURRENCYCODE']) ? $data['CURRENCYCODE'] : $payment_method['settings']['currency_code'];

  /**
   * (Optional) Shipping amount for each billing cycle during this payment period.
   * NOTE: All amounts in the request must have the same currency.
   * Character length and limitations: Value is a positive number which cannot exceed $10,000 USD in any currency. It includes no currency symbol. It must have 2 decimal places, the decimal separator must be a period (.), and the optional thousands separator must be a comma (,).
   */
  $nvp['SHIPPINGAMT'] = isset($data['SHIPPINGAMT']) ? $data['SHIPPINGAMT'] : '0.00';

  /**
   * (Optional) Tax amount for each billing cycle during this payment period.
   * NOTE:All amounts in the request must have the same currency.
   * Character length and limitations: Value is a positive number which cannot exceed $10,000 USD in any currency. It includes no currency symbol. It must have 2 decimal places, the decimal separator must be a period (.), and the optional thousands separator must be a comma (,).
   */
  $nvp['TAXAMT'] = isset($data['TAXAMT']) ? $data['TAXAMT'] : '0.00';

  /**
   * Activation Details Fields
   */

  /**
   * (Optional) Initial non-recurring payment amount due immediately upon profile creation. Use an initial amount for enrolment or set-up fees.
   * NOTE:All amounts included in the request must have the same currency.
   * Character length and limitations: Value is a positive number which cannot exceed $10,000 USD in any currency. It includes no currency symbol. It must have 2 decimal places, the decimal separator must be a period (.), and the optional thousands separator must be a comma (,).
   */
  $nvp['INITAMT'] = isset($data['INITAMT']) ? $data['INITAMT'] : '0.00';

  /**
   * (Optional) Action you can specify when a payment fails. It is one of the following values:
   * - ContinueOnFailure . By default, PayPal suspends the pending profile in the event that the initial payment amount fails. You can override this default behavior by setting this field to ContinueOnFailure. Then, if the initial payment amount fails, PayPal adds the failed payment amount to the outstanding balance for this recurring payment profile.
   *                       When you specify ContinueOnFailure, a success code is returned to you in the CreateRecurringPaymentsProfile response and the recurring payments profile is activated for scheduled billing immediately. You should check your IPN messages or PayPal account for updates of the payment status.
   * - CancelOnFailure . If this field is not set or you set it to CancelOnFailure, PayPal creates the recurring payment profile, but places it into a pending status until the initial payment completes. If the initial payment clears, PayPal notifies you by IPN that the pending profile has been activated. If the payment fails, PayPal notifies you by IPN that the pending profile has been canceled.
   */
  $nvp['FAILEDINITAMTACTION'] = isset($data['FAILEDINITAMTACTION']) ? $data['FAILEDINITAMTACTION'] : 'CancelOnFailure';

  /**
   * Ship To Address Fields
   */

  /**
   * Person's name associated with this shipping address. It is required if using a shipping address.
   * Character length and limitations: 32 single-byte characters
   */
  $nvp['SHIPTONAME'] = isset($data['SHIPTONAME']) ? $data['SHIPTONAME'] : '';

  /**
   * First street address. It is required if using a shipping address.
   * Character length and limitations: 100 single-byte characters
   */
  $nvp['SHIPTOSTREET'] = isset($data['SHIPTOSTREET']) ? $data['SHIPTOSTREET'] : '';

  /**
   * (Optional) Second street address.
   * Character length and limitations: 100 single-byte characters
   */
  $nvp['SHIPTOSTREET2'] = isset($data['SHIPTOSTREET2']) ? $data['SHIPTOSTREET2'] : '';

  /**
   * Name of city. It is required if using a shipping address.
   * Character length and limitations: 40 single-byte characters
   */
  $nvp['SHIPTOCITY'] = isset($data['SHIPTOCITY']) ? $data['SHIPTOCITY'] : '';

  /**
   * State or province. It is required if using a shipping address.
   * Character length and limitations: 40 single-byte characters
   */
  $nvp['SHIPTOSTATE'] = isset($data['SHIPTOSTATE']) ? $data['SHIPTOSTATE'] : '';

  /**
   * U.S. ZIP code or other country-specific postal code. It is required if using a U.S. shipping address; may be required for other countries.
   * Character length and limitations: 20 single-byte characters
   */
  $nvp['SHIPTOZIP'] = isset($data['SHIPTOZIP']) ? $data['SHIPTOZIP'] : '';

  /**
   * Country code. It is required if using a shipping address.
   * Character length and limitations: 2 single-byte characters
   */
  $nvp['SHIPTOCOUNTRY'] = isset($data['SHIPTOCOUNTRY']) ? $data['SHIPTOCOUNTRY'] : '';

  /**
   * (Optional) Phone number.
   * Character length and limitations: 20 single-byte characters
   */
  $nvp['SHIPTOPHONENUM'] = isset($data['SHIPTOPHONENUM']) ? $data['SHIPTOPHONENUM'] : '';


  /**
   * Credit Card Details Fields
   */

  /**
   * (Required) Type of credit card. For UK, only Maestro, MasterCard, Discover, and Visa are allowable. For Canada, only MasterCard and Visa are allowable and Interac debit cards are not supported. It is one of the following values:
   * - Visa, MasterCard, Discover, Amex, Maestro: See note.
   * NOTE:If the credit card type is Maestro, you must set CURRENCYCODE to GBP. In addition, you must specify either STARTDATE or ISSUENUMBER.
   * Character length and limitations: Up to 10 single-byte alphabetic characters
   */
  $nvp['CREDITCARDTYPE'] = isset($data['CREDITCARDTYPE']) ? $data['CREDITCARDTYPE'] : $pane_values['credit_card']['type'];

  /**
   * (Required) Credit card number.
   * Character length and limitations: Numeric characters only with no spaces or punctutation. The string must conform with modulo and length required by each credit card type.
   */
  $nvp['ACCT'] = isset($data['ACCT']) ? $data['ACCT'] : $pane_values['credit_card']['number'];

  /**
   * Credit card expiration date. This field is required if you are using recurring payments with direct payments.
   * Character length and limitations: 6 single-byte alphanumeric characters, including leading zero, in the format MMYYYY
   * Add the start date and issue number if processing a Maestro or Solo card.
   */
  $nvp['EXPDATE'] = isset($data['EXPDATE']) ? $data['EXPDATE'] : $pane_values['credit_card']['exp_month'] . $pane_values['credit_card']['exp_year'];

  /**
   * Card Verification Value, version 2. Your Merchant Account settings determine whether this field is required. To comply with credit card processing regulations, you must not store this value after a transaction has been completed.
   * Character length and limitations: For Visa, MasterCard, and Discover, the value is exactly 3 digits. For American Express, the value is exactly 4 digits.
   */
  if (in_array($pane_values['credit_card']['type'], array('maestro', 'solo'))) {
    if (!empty($pane_values['credit_card']['start_month']) && !empty($pane_values['credit_card']['start_year'])) {
      $nvp['STARTDATE'] = $pane_values['credit_card']['start_month'] . $pane_values['credit_card']['start_year'];
      // (Optional) Month and year that Maestro card was issued.
      // Character length and limitations: Must be 6 digits, including leading zero, in the format MMYYYY
    }

    if (!empty($pane_values['credit_card']['issue'])) {
      $nvp['ISSUENUMBER'] = $pane_values['credit_card']['issue'];
      // (Optional) Issue number of Maestro card.
      // Character length and limitations: 2 numeric digits maximum
    }
  }

  /**
   * Add the CVV if entered on the form.
   */
  if (isset($pane_values['credit_card']['code'])) {
    $nvp['CVV2'] = $pane_values['credit_card']['code'];
  }


  /**
   * Payer Information Fields
   */

  /**
   * (Required) Email address of buyer.
   * Character length and limitations: 127 single-byte characters
   */
  $nvp['EMAIL'] = isset($data['EMAIL']) ? $data['EMAIL'] : substr($order->mail, 0, 127);

  /**
   * (Optional) Unique PayPal Customer Account identification number.
   * Character length and limitations:13 single-byte alphanumeric characters
   */
  //$nvp['PAYERID'] = isset($data['PAYERID']) ? $data['PAYERID'] : '';

  /**
   * (Optional) Status of buyer. It is one of the following values:
   * - verified
   * - unverified
   * Character length and limitations: 10 single-byte alphabetic characters
   */
  //$nvp['PAYERSTATUS'] = isset($data['PAYERSTATUS']) ? $data['PAYERSTATUS'] : '';

  /**
   * (Optional) Buyer.s country of residence in the form of ISO standard 3166 two-character country codes.
   * Character length and limitations: 2 single-byte characters
   */
  //$nvp['COUNTRYCODE'] = isset($data['COUNTRYCODE']) ? $data['COUNTRYCODE'] : '';

  /**
   * (Optional) Buyer.s business name.
   * Character length and limitations: 127 single-byte characters
   */
  //$nvp['BUSINESS'] = isset($data['BUSINESS']) ? $data['BUSINESS'] : '';


  /**
   * Payer Name Fields
   */

  /**
   * (Optional) Buyer.s salutation.
   * Character length and limitations: 20 single-byte characters
   */
  //$nvp['SALUTATION'] = isset($data['SALUTATION']) ? $data['SALUTATION'] : '';

  /**
   * (Optional) Buyer.s first name.
   * Character length and limitations: 25 single-byte characters
   */
  $nvp['FIRSTNAME'] = isset($data['FIRSTNAME']) ? $data['FIRSTNAME'] : substr($billing_address['first_name'], 0, 25);

  /**
   * (Optional) Buyer's middle name.
   * Character length and limitations: 25 single-byte characters
   */
  //$nvp['MIDDLENAME'] = isset($data['MIDDLENAME']) ? $data['MIDDLENAME'] : '';

  /**
   * (Optional) Buyer.s last name.
   * Character length and limitations: 25 single-byte characters
   */
  $nvp['LASTNAME'] = isset($data['LASTNAME']) ? $data['LASTNAME'] : substr($billing_address['last_name'], 0, 25);

  /**
   * (Optional) Buyer's suffix.
   * Character length and limitations: 12 single-byte characters
   */
  //$nvp['SUFFIX'] = isset($data['SUFFIX']) ? $data['SUFFIX'] : '';

  /**
   * Address Fields
   */

  /**
   * (Required) First street address.
   * Character length and limitations: 100 single-byte characters
   */
  $nvp['STREET'] = isset($data['STREET']) ? $data['STREET'] : substr($billing_address['thoroughfare'], 0, 100);

  /**
   * (Optional) Second street address.
   * Character length and limitations: 100 single-byte characters
   */
  $nvp['STREET2'] = isset($data['STREET2']) ? $data['STREET2'] : substr($billing_address['premise'], 0, 100);

  /**
   * (Required) Name of city.
   * Character length and limitations: 40 single-byte characters
   */
  $nvp['CITY'] = isset($data['CITY']) ? $data['CITY'] : substr($billing_address['locality'], 0, 40);

  /**
   * (Required) State or province.
   * Character length and limitations: 40 single-byte characters
   */
  $nvp['STATE'] = isset($data['STATE']) ? $data['STATE'] : substr($billing_address['administrative_area'], 0, 40);

  /**
   * (Required) Country code.
   * Character length and limitationst: 2 single-byte characters
   */
  $nvp['COUNTRYCODE'] = isset($data['COUNTRYCODE']) ? $data['COUNTRYCODE'] : $billing_address['country'];

  /**
   * (Required) U.S. ZIP code or other country-specific postal code.
   * Character length and limitations: 20 single-byte characters
   */
  $nvp['ZIP'] = isset($data['ZIP']) ? $data['ZIP'] : substr($billing_address['postal_code'], 0, 20);

  /**
   * (Optional) Phone number.
   * Character length and limitations: 20 single-byte characters
   */
  //$nvp['SHIPTOPHONENUM'] = isset($data['SHIPTOPHONENUM']) ? $data['SHIPTOPHONENUM'] : '';


  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_authorization_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoAuthorization

  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_capture_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_BillOutstandingAmount

  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_direct_payment_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_express_checkout_payment_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_nonreferenced_credit_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_reauthorization_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_reference_transaction_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_do_void_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_get_balance_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_get_billing_agreement_customer_details_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_get_express_checkout_details_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_get_recurring_payments_profile_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_get_transaction_details_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_manage_pending_recurring_payments_profile_status_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_manage_pending_transaction_status_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_mass_payment_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_refund_transaction_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_set_customer_billing_agreement_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_set_express_checkout_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_transaction_search_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_update_recurring_payments_profile_request($data) {
  // prep data
  //TODO: implement Business logic
  $nvp = array();

  //TODO: implement service call: https://cms.paypal.com/us/cgi-bin/?cmd=_render-content&content_ID=developer/e_howto_api_nvp_r_DoCapture

  return $nvp;
}

function commerce_paypal_wpp_rewrite_avs_response_codes($incoming_code){
  switch($incoming_code) {
    case 'A':
    case 'B':
      return 'Address only (no ZIP code)';
    case 'C':
    case 'E':
    case 'N':
      return 'The transaction is declined';
    case 'D':
    case 'F':
    case 'M':
    case 'X':
      return 'Address and Postal Code';
    case 'G':
      return 'Global Unavailable)';
    case 'I':
      return 'International Unavailable';
    case 'P':
      return 'Postal Code only (no Address)';
    case 'R':
      return 'Retry';
    case 'S':
      return 'Service not Supported';
    case 'U':
      return 'Unavailable';
    case 'W':
      return 'Nine-digit ZIP code (no Address)';
    case 'Y':
      return 'Address and five-digit ZIP';
    case 'Z':
      return 'Five-digit ZIP code (no Address)';
    default:
      return 'Error';
  }
}

/**
 * Submits a PayPal WPP API request to PayPal.
 *
 * @param $payment_method
 *   The payment method instance array associated with this API request.
 * @param $nvp
 *   The set of name-value pairs describing the transaction to submit.
 */
function commerce_paypal_wpp_rewrite_request($payment_method, $nvp = array()) {
  // Get the API endpoint URL for the method's transaction mode.
  $url = commerce_paypal_wpp_rewrite_server_url($payment_method['settings']['server']);

  // Add the default name-value pairs to the array.
  $nvp += array(
    // API credentials
    'USER' => $payment_method['settings']['api_username'],
    'PWD' => $payment_method['settings']['api_password'],
    'SIGNATURE' => $payment_method['settings']['api_signature'],
    'VERSION' => '76.0',
  );

  // Allow modules to alter parameters of the API request.
  drupal_alter('commerce_paypal_wpp_rewrite_request', $nvp);

  // Log the request if specified.
  if ($payment_method['settings']['log']['request'] == 'request') {
    // Mask the credit card number and CVV.
    $log_nvp = $nvp;
    $log_nvp['PWD'] = str_repeat('X', strlen($log_nvp['PWD']));
    $log_nvp['SIGNATURE'] = str_repeat('X', strlen($log_nvp['SIGNATURE']));

    if (!empty($log_nvp['ACCT'])) {
      $log_nvp['ACCT'] = str_repeat('X', strlen($log_nvp['ACCT']) - 4) . substr($log_nvp['ACCT'], -4);
    }

    if (!empty($log_nvp['CVV2'])) {
      $log_nvp['CVV2'] = str_repeat('X', strlen($log_nvp['CVV2']));
    }

    watchdog('commerce_paypal', 'PayPal WPP request to @url: !param', array('@url' => $url, '!param' => '<pre>' . check_plain(print_r($log_nvp, TRUE)) . '</pre>'), WATCHDOG_DEBUG);
  }

  // Prepare the name-value pair array to be sent as a string.
  $pairs = array();

  foreach ($nvp as $key => $value) {
    $pairs[] = $key . '=' . urlencode($value);
  }

  // Setup the cURL request.
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_VERBOSE, 0);
  curl_setopt($ch, CURLOPT_POST, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, implode('&', $pairs));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
  curl_setopt($ch, CURLOPT_NOPROGRESS, 1);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
  $result = curl_exec($ch);

  // Log any errors to the watchdog.
  if ($error = curl_error($ch)) {
    watchdog('commerce_paypal', 'cURL error: @error', array('@error' => $error), WATCHDOG_ERROR);
    return FALSE;
  }
  curl_close($ch);

  // Make the response an array.
  $response = array();

  foreach (explode('&', $result) as $nvp) {
    list($key, $value) = explode('=', $nvp);
    $response[urldecode($key)] = urldecode($value);
  }

  // Log the response if specified.
  if ($payment_method['settings']['log']['response'] == 'response') {
    watchdog('commerce_paypal', 'PayPal WPP response: !param', array('!param' => '<pre>' . check_plain(print_r($response, TRUE)) . '</pre>', WATCHDOG_DEBUG));
  }

  return $response;
}

/**
 * Returns the URL to the specified PayPal WPP server.
 *
 * @param $server
 *   Either sandbox or live indicating which server to get the URL for.
 *
 * @return
 *   The URL to use to submit requests to the PayPal WPP server.
 */
function commerce_paypal_wpp_rewrite_server_url($server) {
  switch ($server) {
    case 'sandbox':
      return 'https://api-3t.sandbox.paypal.com/nvp';
    case 'live':
      return 'https://api-3t.paypal.com/nvp';
  }
}

/**
 * Returns the relevant PayPal payment action for a given transaction type.
 *
 * @param $txn_type
 *   The type of transaction whose payment action should be returned; currently
 *   supports COMMERCE_CREDIT_AUTH_CAPTURE and COMMERCE_CREDIT_AUTH_ONLY.
 */
function commerce_paypal_wpp_rewrite_payment_action($txn_type) {
  switch ($txn_type) {
    case COMMERCE_CREDIT_AUTH_ONLY:
      return 'Authorization';
    case COMMERCE_CREDIT_AUTH_CAPTURE:
      return 'Sale';
  }
}

/**
 * Returns the description of a transaction type for a PayPal WPP payment action.
 */
function commerce_paypal_wpp_rewrite_reverse_payment_action($payment_action) {
  switch (strtoupper($payment_action)) {
    case 'AUTHORIZATION':
      return t('Authorization only');
    case 'SALE':
      return t('Authorization and capture');
  }
}

/**
 * Returns the value for a credit card type expected by PayPal.
 */
function commerce_paypal_wpp_rewrite_card_type($card_type) {
  switch ($card_type) {
    case 'visa':
      return 'Visa';
    case 'mastercard':
      return 'MasterCard';
    case 'amex':
      return 'Amex';
    case 'discover':
      return 'Discover';
    case 'maestro':
      return 'Maestro';
    case 'solo':
      return 'Solo';
  }
}

/**
 * Returns an array of all possible language codes.
 */
function commerce_paypal_wpp_rewrite_languages() {
  return drupal_map_assoc(array('AU', 'DE', 'FR', 'IT', 'GB', 'ES', 'US'));
}
