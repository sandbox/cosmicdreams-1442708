<?php
/**
 * Callbacks that can be implemented
 * function hook_settings_form(){}
 * function hook_submit_form(){}
 * function hook_submit_form_validate(){}
 * function hook_submit_form_submit(){}
 * function hook_redirect_form(){}
 * function hook_redirect_form_validate(){}
 * function hook_redirect_form_validate(){}
 */

/**
 * @file
 * Implements PayPal Website Payments Pro in Drupal Commerce checkout.
 */

/**
 * Payment method callback: settings form.
 */

/**
 * Payment method callback: checkout form.
 */
function commerce_paypal_wpp_rewrite_submit_form($payment_method, $pane_values, $checkout_pane, $order) {  
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  dpm($payment_method, "Payment Method");
  dpm($pane_values, "Pane Values");
  dpm($checkout_pane, "Checkout Pane");
  dpm($order, "Order");

  $payment_method['settings'] += commerce_paypal_wpp_default_settings();

  // Prepare the fields to include on the credit card form.
  $fields = array();

  // Include the card security code field if specified.
  if ($payment_method['settings']['code']) {
    $fields['code'] = '';
  }

  // Add the credit card types array if necessary.
  $card_types = array_diff(array_values($payment_method['settings']['card_types']), array(0));

  if (!empty($card_types)) {
    $fields['type'] = $card_types;
  }

  // Add the start date and issue number if processing a Maestro or Solo card.
  if (in_array('maestro', $card_types) || in_array('solo', $card_types)) {
    $fields += array(
      'start_month' => '',
      'start_year' => '',
      'issue' => '',
    );
  }

  return commerce_payment_credit_card_form($fields);
}

/**
 * Payment method callback: checkout form validation.
 */
function commerce_paypal_wpp_rewrite_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  // Validate the credit card fields.
  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );

  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    return FALSE;
  }
}

/**
 * Payment method callback: checkout form submission.
 */
function commerce_paypal_wpp_rewrite_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  module_load_include('inc', 'commerce_paypal_wpp', 'includes/paypal.client.inc');

  $nvp = array();

  // Override any standard implementations of filling in the parameters of the request here
  $params = komando_commerce_paypal_create_recurring_payments_profile_request($nvp, $payment_method, $pane_form, $pane_values, $order, $charge);

  $response = commerce_paypal_wpp_request($payment_method, $params);
}

/**
 * Returns the default settings for the PayPal WPP payment method.
 */
function commerce_paypal_wpp_rewrite_default_settings() {
  $default_currency = variable_get('commerce_default_currency', 'USD');

  return array(
    'api_username' => '',
    'api_password' => '',
    'api_signature' => '',
    'server' => 'sandbox',
    'code' => TRUE,
    'card_types' => drupal_map_assoc(array('visa', 'mastercard', 'amex', 'discover')),
    'currency_code' => in_array($default_currency, array_keys(commerce_paypal_wpp_currencies())) ? $default_currency : 'USD',
    'txn_type' => COMMERCE_CREDIT_AUTH_CAPTURE,
    'log' => array('request' => 0, 'response' => 0),
  );
}

/**
 * Returns an array of all possible currency codes.
 */
function commerce_paypal_wpp_rewrite_currencies() {
  return drupal_map_assoc(array('AUD', 'BRL', 'CAD', 'CHF', 'CZK', 'DKK', 'EUR', 'GBP', 'HKD', 'HUF', 'ILS', 'JPY', 'MXN', 'MYR', 'NOK', 'NZD', 'PHP', 'PLN', 'SEK', 'SGD', 'THB', 'TWD', 'USD'));
}

